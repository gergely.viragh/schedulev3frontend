const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, '.prettierrc.json'), 'utf8')
);

module.exports = {
  extends: [
    'plugin:react/recommended',
    'standard',
    'plugin:react/jsx-runtime',
    'plugin:testing-library/react',
    // 'plugin:jest/all',
    'prettier'
  ],
  parser: '@typescript-eslint/parser',
  plugins: [
    'prettier',
    'react',
    'react-hooks',
    'jsx-a11y',
    '@typescript-eslint'
  ],
  env: {
    jest: true,
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    },
    requireConfigFile: false,
    Options: {
      presets: ['@babel/preset-react']
    }
  },
  rules: {
    'import/no-dynamic-require': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default': 0,
    'import/no-unresolved': 2,
    'import/no-webpack-loader-syntax': 0,
    'import/prefer-default-export': 0,
    'jsx-a11y/aria-props': 2,
    'jsx-a11y/heading-has-content': 0,
    'jsx-a11y/label-has-associated-control': [
      2,
      {
        // NOTE: If this error triggers, either disable it or add
        // your custom components, labels and attributes via these options
        // See https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/label-has-associated-control.md
        controlComponents: ['Input']
      }
    ],
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/mouse-events-have-key-events': 2,
    'jsx-a11y/role-has-required-aria-props': 2,
    'jsx-a11y/role-supports-aria-props': 2,
    'no-confusing-arrow': 0,

    'react/state-in-constructor': [2, 'never'],
    'react/prefer-stateless-function': 2,

    'react/jsx-uses-vars': 2,
    'react/jsx-uses-react': 2,
    'prettier/prettier': ['error', prettierOptions],
    'arrow-body-style': [2, 'as-needed'],
    'class-methods-use-this': 0,
    'import/imports-first': 0,
    'import/newline-after-import': 0,
    'no-new-object': 2,
    'no-loop-func': 2,
    'one-var-declaration-per-line': 2,
    'space-infix-ops': 2,
    'multiline-comment-style': [2, 'separate-lines'],
    // 'newline-per-chained-call': [2, {
    //   ignoreChainWithDepth: 2
    // }],

    'prefer-template': 2,
    'react/destructuring-assignment': 0,
    // 'react-hooks/rules-of-hooks': '1',
    'react/jsx-closing-tag-location': 2,
    'react/forbid-prop-types': 0,
    'react/jsx-first-prop-new-line': [2, 'multiline'],
    'react/jsx-filename-extension': 0,
    'react/jsx-no-target-blank': 0,
    'react/require-default-props': 0,
    'react/require-extension': 0,
    'react/self-closing-comp': 2,
    'react/sort-comp': 0,
    'require-yield': 0,
    'react/prop-types': [2],
    'react/jsx-closing-bracket-location': 2,
    'no-multi-spaces': 2,
    'react/jsx-tag-spacing': 2,

    'react/jsx-fragments': [2],
    'react/no-multi-comp': [
      2,
      {
        ignoreStateless: true
      }
    ],

    'react/no-string-refs': [
      2,
      {
        noTemplateLiterals: true
      }
    ],
    'react/jsx-wrap-multilines': 2,

    // goods after this

    'padding-line-between-statements': [
      2,
      {
        blankLine: 'always',
        prev: '*',
        next: 'return'
      }
    ],
    eqeqeq: [2, 'always'],
    'max-len': [2, 130],
    curly: 2,
    // 'semi-spacing': 2,
    'comma-spacing': 2,
    'brace-style': 2,
    'prefer-destructuring': [
      2,
      {
        array: true,
        object: true
      },
      {
        enforceForRenamedProperties: false
      }
    ],
    camelcase: 2,
    'prefer-arrow-callback': 2,
    // 'no-console': 1,
    '@typescript-eslint/no-unused-vars': 1,
    'react/no-unused-state': 1,
    'no-use-before-define': 2,
    'jsx-quotes': [2, 'prefer-double'],
    quotes: [2, 'single']
  },
  // settings: {
  //   'import/resolver': {
  //     webpack: {
  //       config: './internals/webpack/webpack.prod.js'
  //     }
  //   }
  // }
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    }
  }
};
