// import { useEffect } from 'react';
// import { HubConnectionBuilder } from '@microsoft/signalr';

// function useHubConnection(hubName, callback) {
//   useEffect(() => {
//     const newConnection = new HubConnectionBuilder()
//       .withUrl(`/api/${hubName}`)
//       .withAutomaticReconnect({
//         nextRetryDelayInMilliseconds: retryContext => {
//             if (retryContext.elapsedMilliseconds < 60000) {
//                 return Math.random() * 10000;
//             } else {
//                 return 60000;
//             }
//         }
//       })
//       .build();

//     newConnection.onclose(error => console.log('close', error));
//     newConnection.onreconnecting(error => console.log('onReconnecting', error));
//     newConnection.onreconnected(error => console.log('onReconnected', error));

//     callback(newConnection);
//   }, []);
// }

// export default useHubConnection;
