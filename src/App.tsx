import React from 'react';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Scheduler from './pages/Scheduler';
import Workspace from './pages/Workspace';
import NotFound from './pages/NotFound';
import NavigatorGroup from './components/NavigatorGroup';
import WorkspaceEditor from './pages/WorkspaceEditor';
import ActiveScheduler from './pages/ActiveScheduler';

function App() {
  // TODO: socket connection test
  // const [connection, setConnection] = useState(null);
  // const [time, setTime] = useState(Date.now());

  // useEffect(() => {
  //   const ws = new HubConnectionBuilder()
  //     .withUrl('http://localhost:5000/chatHub')
  //     .build();

  //   setConnection(ws);
  //   ws.on('LoadTest', (data) => {
  //     if (data.prop3 % 10000 === 0) {
  //       console.log((Date.now() - time) / 1000);
  //     }
  //   });
  // }, []);

  // useEffect(() => {
  //   console.log(connection);
  //   if (connection && !connection.connectionStarted) {
  //     console.log('starting');
  //     connection.start();
  //     console.log('started');
  //   }
  //   if (connection && connection.connectionStarted) {
  //     console.log('hello');
  //   }
  // }, [connection]);
  console.log('App rendered');
  // const [selectedContext, setSelectedContext] = useState('workspace');

  return (
    <Routes>
      <Route path="/" element={<NavigatorGroup />} />
      <Route path="/scheduler" element={<Scheduler />} />
      <Route path="/workspace" element={<Workspace />}>
        <Route path="editor" element={<WorkspaceEditor />} />
      </Route>
      <Route path="/scheduler/active-scheduler" element={<ActiveScheduler />} />

      {/* Private Route */}
      {/* <Route
      path="/dashboard"
      element={
        <PrivateRoute>
          <Dashboard />
        </PrivateRoute>
      }
    /> */}
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
