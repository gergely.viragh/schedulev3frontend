import api from './api';

interface ILoginData {
  email: string;
  password: string;
}

export async function getUsers() {
  const { data: users } = await api.get('/api/users?page=2');

  return users;
}

export async function login(loginData: ILoginData) {
  const response = await api.post('/api/login', loginData);

  return response;
}
