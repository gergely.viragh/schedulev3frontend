import axios from 'axios';

const baseURL = 'https://reqres.in/';

const api = axios.create({
  baseURL,
  timeout: 20000
  // withCredentials: true
});

export default api;
