import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

import { WorkspaceProvider } from './contexts/WorkspaceContext';
import App from './App';
import './index.css';
import { ScheduleProvider } from './contexts/ScheduleContext';

<div
  title="ad123y19418482421412421412412"
  hidden={
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true ||
    true
  }
/>;

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <BrowserRouter>
    {/* <React.StrictMode> */}
    <WorkspaceProvider>
      <ScheduleProvider>
        <App />
      </ScheduleProvider>
    </WorkspaceProvider>
    {/* </React.StrictMode> */}
  </BrowserRouter>
);
