import React, { useContext, useEffect, useReducer } from 'react';

import { ScheduleInfo } from '../types/ScheduleInfo';

export const ScheduleStateContext = React.createContext({});

const initialState: ScheduleInfo = {
  liveSchedule: {
    data: ['testData']
  },
  lastModifiedBy: 'Imre Futy'
};

// const initialState: WorkspaceInfo = {
//     resources: [],
//     testTypes: []
//   };

enum ActionType {
  SetScheduleInfo = 'setScheduleInfo',
  RemoveScheduleInfo = 'removeScheduleInfo'
}

interface Action {
  type: ActionType;
  payload: ScheduleInfo;
}

const reducer: React.Reducer<Record<string, unknown>, Action> = (
  _state,
  action
) => {
  switch (action.type) {
    case ActionType.SetScheduleInfo:
      return {
        liveSchedule: action.payload.liveSchedule,
        lastModifiedBy: action.payload.lastModifiedBy
      };
    case ActionType.RemoveScheduleInfo:
      return {
        liveSchedule: initialState.liveSchedule,
        lastModifiedBy: initialState.lastModifiedBy
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

export const ScheduleProvider = ({ children }: any) => {
  let localState = null;
  if (
    typeof localStorage !== 'undefined' &&
    localStorage.getItem('scheduleInfo')
  ) {
    localState = JSON.parse(localStorage.getItem('scheduleInfo') || '');
  }
  const [state, dispatch] = useReducer(reducer, localState || initialState);

  if (typeof localStorage !== 'undefined') {
    useEffect(() => {
      localStorage.setItem('scheduleInfo', JSON.stringify(state));
    }, [state]);
  }

  return (
    <ScheduleStateContext.Provider value={[state, dispatch]}>
      {children}
    </ScheduleStateContext.Provider>
  );
};

export const useScheduleContext: any = () => useContext(ScheduleStateContext);
