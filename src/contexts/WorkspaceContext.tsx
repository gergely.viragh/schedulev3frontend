import React, { useContext, useEffect, useReducer } from 'react';
import { ResourceType } from '../types/DataManagement';

import { WorkspaceInfo } from '../types/WorkspaceInfo';

export const WorkspaceStateContext = React.createContext({});

const initialState: WorkspaceInfo = {
  resources: [
    {
      type: ResourceType.Analyst,
      name: 'Imre Futy'
    },
    {
      type: ResourceType.Machine,
      name: 'BTC miner'
    }
  ],
  testTypes: [
    {
      name: 'Important test'
    }
  ]
};

// const initialState: WorkspaceInfo = {
//     resources: [],
//     testTypes: []
//   };

enum ActionType {
  SetWorkspaceInfo = 'setWorkspaceInfo',
  RemoveWorkspaceInfo = 'removeWorkspaceInfo'
}

interface Action {
  type: ActionType;
  payload: WorkspaceInfo;
}

const reducer: React.Reducer<Record<string, unknown>, Action> = (
  _state,
  action
) => {
  switch (action.type) {
    case ActionType.SetWorkspaceInfo:
      return {
        resources: action.payload.resources,
        testTypes: action.payload.testTypes
      };
    case ActionType.RemoveWorkspaceInfo:
      return {
        resources: initialState.resources,
        testTypes: initialState.testTypes
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

export const WorkspaceProvider = ({ children }: any) => {
  let localState = null;
  if (
    typeof localStorage !== 'undefined' &&
    localStorage.getItem('workspaceInfo')
  ) {
    localState = JSON.parse(localStorage.getItem('workspaceInfo') || '');
  }
  const [state, dispatch] = useReducer(reducer, localState || initialState);

  if (typeof localStorage !== 'undefined') {
    useEffect(() => {
      localStorage.setItem('workspaceInfo', JSON.stringify(state));
    }, [state]);
  }

  return (
    <WorkspaceStateContext.Provider value={[state, dispatch]}>
      {children}
    </WorkspaceStateContext.Provider>
  );
};

export const useWorkspaceContext: any = () => useContext(WorkspaceStateContext);
