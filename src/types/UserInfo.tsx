import { UserWorkspaceRole } from './User.types';

export interface UserInfo {
  name: string;
  workspaceRoles: Array<UserWorkspaceRole>;
}
