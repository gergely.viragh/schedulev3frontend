import { Resource, TestType } from './DataManagement';

export interface WorkspaceInfo {
  resources: Array<Resource>;
  testTypes: Array<TestType>;
}
