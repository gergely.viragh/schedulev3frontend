export interface LiveSchedule {
  data: Array<string>;
}

export interface ScheduleInfo {
  lastModifiedBy: string;
  liveSchedule: LiveSchedule;
}
