export enum WorkspaceRole {
  Scheduler,
  Analyst,
  Tv
}

export interface UserWorkspaceRole {
  workspaceId: number;
  role: WorkspaceRole;
}
