export enum ResourceType {
  Analyst,
  Machine
}

export interface TestType {
  name: string;
}

export interface Resource {
  type: ResourceType;
  name: string;
}
