import React from 'react';
import { useScheduleContext } from '../contexts/ScheduleContext';

function ScheduleContextRenderer() {
  console.log('scheduleContextRenderer rendered');

  const [schedule, setSchedule] = useScheduleContext();
  const setScheduleAction = {
    type: 'setScheduleInfo',
    payload: {
      ...schedule,
      liveSchedule: {
        data: schedule.liveSchedule.data.concat('More Data')
      },
      lastModifiedBy:
        schedule.lastModifiedBy === 'Peter Futy' ? 'Imre Futy' : 'Peter Futy'
    }
  };

  const resetScheduleAction = {
    type: 'removeScheduleInfo'
  };

  return (
    <>
      <div>
        <pre>{JSON.stringify(schedule, null, 2)}</pre>
      </div>
      <button onClick={() => setSchedule(setScheduleAction)}>
        Add data to live schedule
      </button>
      <button onClick={() => setSchedule(resetScheduleAction)}>
        Reset schedule
      </button>
    </>
  );
}

export default ScheduleContextRenderer;
