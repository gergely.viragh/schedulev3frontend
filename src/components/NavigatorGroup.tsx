import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function NavigatorGroup() {
  const navigate = useNavigate();

  return (
    <div>
      <button onClick={() => navigate('/workspace')}>
        Go to workspace Context
      </button>
      <button onClick={() => navigate('/scheduler')}>
        Go to schedule Context
      </button>
    </div>
  );
}
