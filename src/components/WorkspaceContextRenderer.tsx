import React from 'react';
import { useWorkspaceContext } from '../contexts/WorkspaceContext';
import { ResourceType } from '../types/DataManagement';

function WorkspaceContextRenderer() {
  console.log('workspaceContextRendered rendered');

  const [workspace, setWorkspace] = useWorkspaceContext();
  const setWorkspaceUserAction = {
    type: 'setWorkspaceInfo',
    payload: {
      ...workspace,
      resources: [
        ...workspace.resources,
        {
          type: ResourceType.Analyst,
          name: 'Peter Futy'
        }
      ]
    }
  };

  const setWorkspaceMachineAction = {
    type: 'setWorkspaceInfo',
    payload: {
      ...workspace,
      resources: [
        ...workspace.resources,
        {
          type: ResourceType.Machine,
          name: 'SOL miner'
        }
      ]
    }
  };

  const resetWorkspaceAction = {
    type: 'removeWorkspaceInfo'
  };

  return (
    <>
      <div>
        <pre>{JSON.stringify(workspace, null, 2)}</pre>
      </div>
      <button onClick={() => setWorkspace(setWorkspaceUserAction)}>
        Add user
      </button>
      <button onClick={() => setWorkspace(setWorkspaceMachineAction)}>
        Add machine
      </button>
      <button onClick={() => setWorkspace(resetWorkspaceAction)}>
        Reset workspace
      </button>
    </>
  );
}

export default WorkspaceContextRenderer;
