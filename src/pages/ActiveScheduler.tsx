import React from 'react';
import NavigatorGroup from '../components/NavigatorGroup';
import { useScheduleContext } from '../contexts/ScheduleContext';

export default function ActiveScheduler() {
  const [schedule] = useScheduleContext();

  return (
    <div>
      <h1>Active Scheduler</h1>
      <div>
        <pre>{JSON.stringify(schedule, null, 2)}</pre>
      </div>
      <NavigatorGroup />
    </div>
  );
}
