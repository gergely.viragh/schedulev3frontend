import WorkspaceContextRenderer from '../components/WorkspaceContextRenderer';
import React from 'react';
import NavigatorGroup from '../components/NavigatorGroup';
import { Link, Outlet } from 'react-router-dom';

export default function Workspace() {
  return (
    <div>
      <WorkspaceContextRenderer />;
      <NavigatorGroup />
      <Link to="/workspace/editor">Go to workspace editor</Link>
      <Outlet />
    </div>
  );
}
