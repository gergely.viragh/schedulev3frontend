import ScheduleContextRenderer from '../components/ScheduleContextRenderer';
import React from 'react';
import NavigatorGroup from '../components/NavigatorGroup';
import { Link } from 'react-router-dom';

export default function Scheduler() {
  return (
    <div>
      <ScheduleContextRenderer />
      <div>
        <Link to={'/scheduler/active-scheduler'}>Go to active schedule</Link>
      </div>
      <NavigatorGroup />
    </div>
  );
}
